
public class Persona {

	private String nombre;
	private int edad;
	private double estatura;
	/**
	 * 
	 * @return nombre de Persona.
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * Imprime saludo y nombre de Persona.
	 */
	public void saluda() {
		System.out.println("Hola!! Me llamo " + nombre + ". Tengo " +
				edad + " años y soy un objeto de tipo persona.");
	}
	/**
	 * 
	 * @return String nombre, edad y estatura de Persona.
	 */
	public String toString() {
		return "Persona [nombre=" + nombre + ", edad=" + edad + ", estatura=" + estatura + "]";
	}
	/**
	 * 
	 * @param nombre nombre de Persona.
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * 
	 * @return edad de Persona.
	 */
	public int getEdad() {
		return edad;
	}
	/**
	 * 
	 * @param edad edad de Persona. 
	 */
	public void setEdad(int edad) {
		this.edad = edad;
	}
	/**
	 * 
	 * @return estatura de Persona.
	 */
	public double getEstatura() {
		return estatura;
	}
	/**
	 * 
	 * @param estatura estatura de Persona.
	 */
	public void setEstatura(double estatura) {
		this.estatura = estatura;
	}
	/**
	 * Constructor con los parametros de las caracteristicas de Persona. 
	 * @param nombre nombre d ela persona. 
	 * @param edad edad de la perosona.
	 * @param estatura estatura de la persona.
	 */
	public Persona(String nombre, int edad, double estatura) {
		this.nombre = nombre;
		this.edad = edad;
		this.estatura = estatura;
	}
	
}
